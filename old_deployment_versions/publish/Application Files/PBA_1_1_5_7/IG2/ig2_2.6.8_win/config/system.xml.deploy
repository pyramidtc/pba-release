<?xml version="1.0" encoding="utf-8"?>
<system
  xmlns="http://www.ptcusa.com"
  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://www.ptcusa.com/A510.xsd" type="pyramid" >
 
  <hosts>
    <!-- NOTE: the ip address does not matter for bcs, but may be required to be present -->
    <host ip="255.255.255.255" name="PBAHost" localhost="true" />
  </hosts>
  <loopcontrollers>
    <!-- I128 device**************************************************** -->
    <loopcontroller type="I128" name="MLFC_PBA_SERIALNUM" ip="IPADDRESSBLANK" >
      <channels>
        <channel name="SERIALNUM_in_integration_time" wire="int_in_integration_time"/>
        <channel name="SERIALNUM_in_hv" wire="analog_in_external_hv" />
        <channel name="SERIALNUM_in_measuring" wire="digital_in_measuring" />
        <channel name="SERIALNUM_in_current" wire="analog[135]_in_current" aMax="1000"/>
        <channel name="SERIALNUM_out_hv" wire="analog_out_hv" />
        <channel name="SERIALNUM_out_initiate" wire="digital_out_initiate" />
        <channel name="SERIALNUM_integration" wire="analog_out_integration_time" />
        <channel name="SERIALNUM_conversions" wire="int_out_conversions_per_sample" />
        <channel name="SERIALNUM_range" wire="int_out_range_hcc" />
        <channel name="SERIALNUM_hcc" wire="analog_in_hcc" />
        <channel name="SERIALNUM_ion_chamber_mode" wire="int_out_ion_chamber_mode" />
        <channel name="SERIALNUM_averaging_period" wire="analog_out_averaging_period" />
        <channel name="SERIALNUM_offset_vector" wire="analog[258]_out_offset_vector" />
        <channel name="SERIALNUM_clear_offset_vector" wire="digital_out_clear_offset_vector" />
        <channel name="SERIALNUM_enable_external_hv" wire="digital_out_enable_external_hv" />
        <channel name="SERIALNUM_firmware" wire="string_in_firmware" />
        <channel name="SERIALNUM_fpga" wire="string_in_fpga" />
        <channel name="SERIALNUM_hardware" wire="string_in_hardware_rev" />
        <channel name="SERIALNUM_serial" wire="string_in_serial_num" />
        <channel name="SERIALNUM_software" wire="string_in_software_rev" />
        <channel name="SERIALNUM_secondary" wire="string_in_secondary_fpga" />
        <channel name="SERIALNUM_RTP" wire="string_in_rtp_rev" />
        <channel name="SERIALNUM_filter" wire="int_out_filter" />
        <channel name="SERIALNUM_combine_channels" wire="digital_out_combine_channels" />
        <channel name="SERIALNUM_monitor_output" wire="analog_out_monitor_charge" />
        <channel name="SERIALNUM_align_channel_data" wire="digital_out_align_channel_data" />
        <channel name="SERIALNUM_target_dose" wire="analog_out_target_dose" />
        <channel name="SERIALNUM_opt_enable" wire="digital_out_opt_enable" />
        <channel name="SERIALNUM_hcc_data_unfiltered" wire="analog_in_hcc" />
        <channel name="SERIALNUM_hcc_data_filtered" wire="analog_in_hcc_processed" />
        <channel name="SERIALNUM_opt_enabled" wire="digital_in_opt_enabled" />
        <channel name="SERIALNUM_target_reached" wire="digital_in_hcc_target_reached" />
        <channel name="SERIALNUM_dose_accumulated" wire="analog_in_hcc_dose" />
        <channel name="SERIALNUM_start_trig" wire="int_out_start_trigger_source" />
        <channel name="SERIALNUM_pause_trig" wire="int_out_pause_trigger_source" />
        <channel name="SERIALNUM_stop_trig" wire="int_out_stop_trigger_source" />
        <channel name="SERIALNUM_bnc" wire="int_out_bnc_start_gate" />
        <channel name="SERIALNUM_burst" wire="int_out_burst_count" />
        <channel name="SERIALNUM_stop_count" wire="int_out_stop_count" />
        <channel name="SERIALNUM_offset" wire="int_out_register_offset" />
        <channel name="SERIALNUM_contents" wire="int_out_register_contents" />
        <channel name="SERIALNUM_getorset" wire="digital_out_register_command" />
        <channel name="SERIALNUM_base_address" wire="int_out_base_address" />
        <channel name="SERIALNUM_relay_enable" wire="digital_out_relay_enable" />
        <channel name="SERIALNUM_actuator_enable" wire="digital_out_actuator_enable" />
        <channel name="SERIALNUM_test_ab" wire="digital_out_test_ab" />
        <channel name="SERIALNUM_interlock_enable" wire="digital_out_interlock_enable" />
        <channel name="SERIALNUM_interlock_readbacks" wire="int_in_interlock_readbacks" />
        <channel name="SERIALNUM_ic_temp" wire="analog_in_ic_temp" />
        <channel name="SERIALNUM_ic_pressure" wire="analog_in_ic_pressure" />
        <channel name="SERIALNUM_ic_humidity" wire="analog_in_ic_humidity" />
        <channel name="SERIALNUM_ic_reference" wire="analog_in_ic_reference" />
        <channel name="SERIALNUM_dac_1" wire="analog_out_dac_1" />
        <channel name="SERIALNUM_dac_2" wire="analog_out_dac_2" />
        <channel name="SERIALNUM_adc_1" wire="analog_in_adc_1" />
        <channel name="SERIALNUM_adc_2" wire="analog_in_adc_2" />
        <channel name="SERIALNUM_calibration_current" wire="digital_out_calibration_source" />
        <channel name="SERIALNUM_calibration_channel" wire="int_out_calibration_channel" />
        <channel name="SERIALNUM_variant_calibration" wire="analog[260]_out_current_calibration" />
        <channel name="SERIALNUM_calib_channel" wire="int_out_calibration_channel" />
        <channel name="SERIALNUM_clear_errors" wire="digital_out_clear_errors" />
        <channel name="SERIALNUM_set_interlock" wire="digital_out_set_interlock" />
        <channel name="SERIALNUM_configure_xml" wire="string_out_configure_xml" />
        <channel name="SERIALNUM_is_connected" wire="digital_in_connection_status" />
        <channel name="SERIALNUM_ip_address" wire="string_in_ip_address" />
        <channel name="SERIALNUM_buffered_acquisition" wire="digital_out_buffered_acquisition" />
        <channel name="SERIALNUM_buffered_initiate" wire="digital_out_initiate" />
      </channels>
    </loopcontroller>
  </loopcontrollers>
 
  <interpreter>
    <devices>
      <epicscas type="epicscas" name="epics_server"/>
      <detector type="gaussianfit" name="GaussianFit_1"
          hcc="SERIALNUM_hcc"
          current="SERIALNUM_in_current"
          biasrdbk="SERIALNUM_in_hv" biascmd="SERIALNUM_out_hv"
          initiate="SERIALNUM_out_initiate"
          integrationtime="SERIALNUM_in_integration_time"
          signal_threshold_percent="1"
          max_signal_channels="6"
          weighting="2"
          num_peaks="1">
        <channels>
          <channel name="SERIALNUM_position" wire="analog_in_position" />
          <channel name="SERIALNUM_sigma" wire="analog_in_sigma" />
          <channel name="SERIALNUM_amplitude" wire="analog_in_amplitude" />
          <channel name="SERIALNUM_channels" wire="variant_in_channels" />
          <channel name="SERIALNUM_initiate" wire="digital_out_initiate" />
          <channel name="SERIALNUM_gaussian_data" wire="analog[142]_in_gaussian_data" aMax="1000"/>
          <channel name="SERIALNUM_regression_options" wire="analog[5]_out_regression_options" />
        </channels>
      </detector>
    </devices>
  </interpreter>
 
</system>