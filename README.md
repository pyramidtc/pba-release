# PBA

PBA (Pyramid Beam Analyzer) software is a Pyramid product meant to be used with a Pyramid MLRV device.
For more information visit www.ptcusa.com

## Version Notes
### 1.2.0.1
* Fixed installer issue and made installer package location independant

### 1.2.0.0
* Fixed issue with CoG algorithm not using window value correctly

### 1.1.6.6
* Added option for suppressing errant channel 0

### 1.1.6.5
* Hotfix for ip address not saving or changing
* IG2 wasn't closing correctly, causing ghost ig2 instances

### 1.1.6.3
* Moved location of version notes and history

### 1.1.6.2
* Fixed bug where master data wasn't being compared

### 1.1.6.1
* Added MLRV filter compatibility to software. This version can now use version 3 calibration files.